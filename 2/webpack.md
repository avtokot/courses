## Почему webpack?

+ Хорошо работает для SPA
+ Работает с синтаксисом require() и import для загрузки модулей
+ Дает гибкую настройку разделения кода
+ Возможность hot reload для распространенных библиотек и фреймворков
- Сложен в освоении
- Работа с импортированием файлов в проект смущает первое время
- Документация могла быть и лучше
- Большое количество нововведений

### Установка

Рекомендуется устанавливать локально

```npm install webpack --save-dev```

Как только он установлен, проще всего запускать его как скрипт в package.json

```
    "scripts": {
        "build": "webpack -p",
        "watch": "webpack --watch"
    },
```

### 2. Webpack Config File

В корне папки проекта нужно создать файл webpack.config.js

```
var path = require('path');

module.exports = {
  entry: './assets/js/index.js', // файл который нужно обработать
  output: {
    filename: 'bundle.js', // имя файла
    path: path.resolve(__dirname, 'dist') // путь файла который будет собран
  }
};
```
```
<script src="./dist/bundle.js"></script>
```

Этого минимальная настройка для работы сборщика.

### 3. Модульность

Создадим файл greeter.js

```
function greet() {
    console.log('Have a great day!');
};

export default greet;
```
Чтобы использовать эту функцию ее нужно импортировать в наш главный файл index.js

```
import greet from './greeter.js';

console.log("I'm the entry point");
greet();
```

### 4. Подключение библиотек

Сперва нужно установить библиотеку через пакетный менеджер

```npm install moment --save```

Затем подключить его в файл greeter.js
```
import moment from 'moment';

function greet() {
    var day = moment().format('dddd');
    console.log('Have a great ' + day + '!');
};

export default greet;
```

### 5. Загрузчики

Загрузчики это внешние комопненты, которые webpack использует для преобразования файлов.

```npm install jshint jshint-loader --save-dev```

И после установки нужно добавить настройки в webpack.config.js

```
var path = require('path');

module.exports = {
  entry: './assets/js/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  // Add the JSHint loader
  module: {
    rules: [{
      test: /\.js$/, // Выполнить загрузчик на всех файлах .js
      exclude: /node_modules/, // игнорировать все в папке node_modules
      use: 'jshint-loader'
    }]
  }
};
```

[webpack.js.org](http://webpack.js.org/) - Официальный сайт проекта
[Awesome webpack](https://github.com/webpack-contrib/awesome-webpack) - Список ресурсов по webpack
[Webpack 2](https://www.youtube.com/watch?v=eWmkBNBTbMM) - Двух часовой видео-урок
[Webpack Examples](https://github.com/webpack/webpack/tree/master/examples) - Примеры настроек
