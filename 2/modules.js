/**
 * Модули es6
 */

// Модулем считается файл с кодом который экспортируется каким либо образом

// Экпортировать можно любой тип из js
export let one = 1;

export function sayHi() {
    alert("Hello!");
}

// Модули можно включать в код приложения через import
import {one, sayHi} from "./nums";

console.log(one);
sayHi();

// Если файл выполняет только одну вещь то можно использовать экспорт по умолчанию
export default str = 'Some string';
