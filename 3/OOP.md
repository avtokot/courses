# Объектно-ориентированное программирование

ООП способствует большей гибкости и поддерживаемости в программировании, и широко распространена в крупномасштабном программном инжиниринге.

```
var Person = function (firstName) {
    this.firstName = firstName;
};

Person.prototype.sayHello = function() {
    console.log("Hello, I'm " + this.firstName);
};

var person1 = new Person("Alice");
var person2 = new Person("Bob");

// вызываем метод sayHello() класса Person
person1.sayHello(); // выведет "Hello, I'm Alice"
person2.sayHello(); // выведет "Hello, I'm Bob"
```

```
class Person {
    constructor(name) {
        this.firstName = firstName;
    }

    sayHello() {
        console.log("Hello, I'm " + this.firstName);
    }
}

var person1 = new Person("Alice");
var person2 = new Person("Bob");

// вызываем метод sayHello() класса Person
person1.sayHello(); // выведет "Hello, I'm Alice"
person2.sayHello(); // выведет "Hello, I'm Bob"
```

```
class Student extends Person {
    ...
    sayGoodBye() {
        console.log("Goodbye!");
    }
}
```

```
Student.prototype.sayGoodBye = function(){
  console.log("Goodbye!");
};

Student.prototype = Object.create(Person.prototype);
```

