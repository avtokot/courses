/**
 * Оставшиеся параметры (rest parameters).
 * Оператор расширения (spread operator).
 */

// Оператор «spread»

var arr = ['will', 'love'];
var data = ['You', ...arr, 'spread', 'operator'];
console.log(data); // ['You', 'will', 'love', 'spread', 'operator']

// Копирование а не ссылки

var arr = [1, 2, 3, 4, 5];
var data = [...arr];
var copy = arr;

arr === data; // false − ссылки отличаются − два разных массива
arr === copy; // true − две переменные ссылаются на один массив

// Чтобы применить массив к конструктору Date нужно

new (Date.bind.apply(Date, [null].concat([1993, 3, 24]))); // 24 Апреля 1993 года

// Оператор spread помогает изюежать лишнего кода

var day = [1993, 3, 24];
var birthday = new Date(...day);

// Оператор «rest»

var log = function(a, b, ...rest) {
    console.log(a, b, rest);
};

log('Basic', 'rest', 'operator', 'usage'); // Basic rest ['operator', usage]

// Объекты можно деструктурировать и каждое из значений объекта будет доступно в виде переменной

let options = {
    title: "Меню",
    width: 100,
    height: 200
};

let {title, width, height} = options;

alert(title); // Меню
alert(width); // 100
alert(height); // 200

//  Клонирование и объединение объектов

var obj1 = {foo: 'bar', x: 42};
var obj2 = {foo: 'baz', y: 13};

var clonedObj = {...obj1}; // Object { foo: "bar", x: 42 }
var mergedObj = {...obj1, ...obj2}; // Object { foo: "baz", x: 42, y: 13 }
